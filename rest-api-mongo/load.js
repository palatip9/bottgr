const tf = require("@tensorflow/tfjs");
require("@tensorflow/tfjs-node");
const csv = require("fast-csv");
const bodyParser = require('body-parser')


async function main() {
MAX = 1200;
var model = await tf.loadModel('file://model/model.json');
  // console.log(data_input[0]);
  test_input = [[
    0/MAX,0/MAX,0/MAX,0/MAX,0/MAX,75/MAX,65/MAX,59/MAX,77/MAX,84/MAX,229/MAX,98/MAX
  ]];
  var r = model.predict(tf.reshape(tf.tensor2d(test_input), [-1, 12, 1]));
  var result1 = r.dataSync()[0];
  // console.log(75/MAX);
  console.log(result1*MAX);

  test_input = [[
    0/MAX,0/MAX,0/MAX,0/MAX,75/MAX,65/MAX,59/MAX,77/MAX,84/MAX,229/MAX,98/MAX,result1
  ]];
   r = model.predict(tf.reshape(tf.tensor2d(test_input), [-1, 12, 1]));
   var result2 = r.dataSync()[0];
  // console.log(75/MAX);
  console.log(result2*MAX);

  test_input = [[
    0/MAX,0/MAX,0/MAX,75/MAX,65/MAX,59/MAX,77/MAX,84/MAX,229/MAX,98/MAX,result1,result2
  ]];
   r = model.predict(tf.reshape(tf.tensor2d(test_input), [-1, 12, 1]));
   var result = r.dataSync()[0];
  // console.log(75/MAX);
  console.log(result*MAX);
}
main();
